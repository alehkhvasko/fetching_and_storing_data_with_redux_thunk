import {
  FETCH_DATA_START,
  FETCH_DATA_ERROR,
  FETCH_DATA_SUCCESS,
} from "./fetchTypes";

const initialState = {
  data: null,
  error: null,
  isLoading: false,
};

export const fetchReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA_START:
      return { ...state, isLoading: true };
    case FETCH_DATA_SUCCESS:
      return { ...state, data: action.payload, error: null, isLoading: false };
    case FETCH_DATA_ERROR:
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
};
