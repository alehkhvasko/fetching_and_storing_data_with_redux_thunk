import {
  FETCH_DATA_START,
  FETCH_DATA_ERROR,
  FETCH_DATA_SUCCESS,
} from "./fetchTypes";

export const fetchActions = (url) => {
  return (dispatch) => {
    dispatch({ type: FETCH_DATA_START });
    fetch(url)
      .then((response) => {
        if (!response.ok) throw new Error("Failed to fetch data");
        return response.json();
      })
      .then((data) => {
        dispatch({ type: FETCH_DATA_SUCCESS, payload: data });
      })
      .catch((error) => {
        dispatch({ type: FETCH_DATA_ERROR, payload: error });
      });
  };
};
