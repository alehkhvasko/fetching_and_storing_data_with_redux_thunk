import { useFetchAPi } from "../fetchApi/fetchAPi";

export const PostList = () => {
  const { data, error, isLoading } = useFetchAPi(
    "https://jsonplaceholder.typicode.com/posts"
  );

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <div>
          <h1>Post List</h1>
          <ul style={{ textAlign: "left" }}>
            {data?.map((posts, index) => (
              <li style={{ listStyleType: "none" }} key={posts.id}>
                {index + 1}) {posts.title}
                <br />
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

// export const useFetch = (url) => {
//   const dispatch = useDispatch();

//   useEffect(() => {
//     dispatch(fetchData(url));
//   }, [dispatch, url]);

//   const { data, error, isLoading } = useSelector((state) => state.data);

//   return { data, error, isLoading };
// };
