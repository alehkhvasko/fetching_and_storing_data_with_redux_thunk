import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchActions } from "../../store/fetchApi/fetchActions";

export const useFetchAPi = (url) => {
  console.log("from fetchApi");
  const dispatch = useDispatch();
  const data = useSelector((state) => state.data);
  useEffect(() => {
    dispatch(fetchActions(url));
  }, [dispatch, url]);
  return data;
};
