import { Link } from "react-router-dom";
import "./Navigation.scss";
import { AppRoutes } from "../../AppRoutes";

export const Navigation = () => {
  const { Post, ToDo, User } = AppRoutes;
  return (
    <nav>
      <ul>
        <li>
          <Link className="link" to={Post}>
            PostList
          </Link>
        </li>
      </ul>
      <ul>
        <li>
          <Link className="link" to={ToDo}>
            ToDoList
          </Link>
        </li>
      </ul>
      <ul>
        <li>
          <Link className="link" to={User}>
            UserList
          </Link>
        </li>
      </ul>
    </nav>
  );
};
