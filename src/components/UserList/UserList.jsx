import { useFetchAPi } from "../fetchApi/fetchAPi";

export const UserList = () => {
  const { data, error, isLoading } = useFetchAPi(
    "https://jsonplaceholder.typicode.com/users"
  );
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <div>
          <h1>To Do List </h1>
          {data?.map((user) => (
            <li style={{ listStyleType: "none" }} key={user.id}>
              Name: {user.name}.<br /> (Username: {user.username}.)
            </li>
          ))}
        </div>
      )}
    </div>
  );
};
